/******************************************************************
 *
 * SLIDE JS
 *
 ******************************************************************/
$(function() {

    // “wrapper”クラスの幅を取得し、画像に指定
    var wrapperSize = $('#wrapper').width();
    $('.slideChild, .slideChild img').width( wrapperSize );

    var slideWidth = wrapperSize;
    var slideNum = $('.slideChild').length;
    var slideTotalWidth = slideWidth * slideNum;
    var setIntervalId = 0;
    $('.slideParent').css('width', slideTotalWidth);

    var slideCurrent = 0;

    // アニメーション
    var sliding = function() {

        if( slideCurrent < 0 ) {
            slideCurrent = slideNum - 1;
        } else if( slideCurrent > slideNum - 1 ) {
            slideCurrent = 0;
        }
        console.log(slideCurrent);

        $('.slideParent').animate({
            left: slideCurrent * -slideWidth
        });

    };

    // アニメーションインターバル
    var slideAnimation = function() {

        // アニメーションインターバル
        setIntervalId = setInterval(function() {

            slideCurrent++;
            sliding();

        }, 3000);
    };

    // インターバルクリア後、再開
    var resetInterval = function() {

        clearInterval( setIntervalId );
        slideAnimation();

    };

    // 次へボタン
    $('.slideNext').click(function() {
        resetInterval();
        slideCurrent++;
        sliding();
    });

    // 前へボタン
    $('.slidePrev').click(function() {
        resetInterval();
        slideCurrent--;
        sliding();
    });


    // 初回アニメーション
    slideAnimation();

});
